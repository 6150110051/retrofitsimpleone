package com.example.retrofitsimpleone;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
public class Customer {
    @SerializedName("Name")
    @Expose
    private String name;
    @SerializedName("City")
    @Expose
    private String city;
    @SerializedName("Country")
    @Expose
    private String country;
    public Customer(String name, String city, String country) {
        this.name = name;
        this.city = city;
        this.country = country;
    }
    public String getName() {
        return name;
    }
    public void setName(String name) {
        this.name = name;
    }
    public String getCity() {
        return city;
    }
    public void setCity(String city) {
        this.city = city;
    }
    public String getCountry() {
        return country;
    }
    public void setCountry(String country) {
        this.country = country;
    }
}
